﻿using Microsoft.EntityFrameworkCore;

namespace ToDoAppBackend.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly DataContext _dataContext;

        public UserService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<User?> GetUserById(int id)
        {
            var user = await _dataContext.Users.FindAsync(id);
            if (user is null)
                return null;
            return user;
        }

        public async Task<User?> GetUserByUsername(string userName)
        {
            var user = await _dataContext.Users.Where(s => s.UserName == userName).FirstOrDefaultAsync();
            if (user is null)
                return null;
            return user;
        }

        public async Task<List<User>> GetUsers()
        {
            var users = await _dataContext.Users.ToListAsync();
            return users;
        }

        public async Task<User> AddUser(User user)
        {

            _dataContext.Users.Add(user);
            await _dataContext.SaveChangesAsync();
            return user;
        }

        public async Task<User?> DeleteUser(int userId)
        {
            var user = await _dataContext.Users.FindAsync(userId);
            if (user is null)
            {
                return null;
            }
            _dataContext.Users.Remove(user);
            await _dataContext.SaveChangesAsync();

            return user;
        }

        public async Task<User?> UpdateUser(int id, User request)
        {
            var user = await _dataContext.Users.FindAsync(id);
            if (user is null)
                return null;

            user.Password = request.Password;

            await _dataContext.SaveChangesAsync();

            return user;
        }

        public async Task<bool> Exists(int id)
        {
            var user = await _dataContext.Users.FindAsync(id);
            if (user is null)
                return false;
            return true;
        }

        public async Task<bool> ExistsByUserName(string userName)
        {
            var user = await _dataContext.Users.Where(s => s.UserName == userName).FirstOrDefaultAsync();
            if (user is null)
                return false;
            return true;
        }
    }
}
