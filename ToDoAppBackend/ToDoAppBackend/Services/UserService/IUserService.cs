﻿namespace ToDoAppBackend.Services.UserService
{
    public interface IUserService
    {
        Task<List<User>> GetUsers();

        Task<User?> GetUserById(int id);

        Task<User?> GetUserByUsername(string userName);

        Task<User> AddUser(User user);

        Task<User?> UpdateUser(int id, User user);

        Task<User?> DeleteUser(int userId);

        Task<bool> Exists(int id);

        Task<bool> ExistsByUserName(String userName);
    }
}
