﻿using Microsoft.EntityFrameworkCore;

namespace ToDoAppBackend.Services.ItemService
{
    public class ItemService : IItemService
    {
        private readonly DataContext _datacontext;

        public ItemService(DataContext dataContext)
        {
            _datacontext = dataContext;
        }

        public async Task<Item> AddItem(Item item)
        {
            _datacontext.Items.Add(item);
            await _datacontext.SaveChangesAsync();
            return item;
        }

        public async Task<Item?> DeleteItem(int itemId)
        {
            var item = await _datacontext.Items.FindAsync(itemId);
            if (item is null) {
                return null;
            }

            _datacontext.Items.Remove(item);
            await _datacontext.SaveChangesAsync();
            return item;
        }

        public async Task<Item?> GetItem(int id)
        {
            var item = await _datacontext.Items.FindAsync(id);
            if (item is null)
            {
                return null;
            }
            return item;
        }

        public async Task<List<Item>> GetItemsByGroupId(int groupId)
        {
            var item = await _datacontext.Items.Where(i => i.GroupId == groupId).ToListAsync();
            return item;
        }

        public async Task<Item?> UpdateItem(int id, Item request)
        {
            var item = await _datacontext.Items.FindAsync(id);
            if (item is null)
            {
                return null;
            }
            item.Name = request.Name;
            item.Description = request.Description;
            item.DueDate = request.DueDate;
            item.IsCompleted = request.IsCompleted;

            await _datacontext.SaveChangesAsync();
            return item;
        }
    }
}
