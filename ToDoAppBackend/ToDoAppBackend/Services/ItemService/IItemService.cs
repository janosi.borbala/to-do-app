﻿
namespace ToDoAppBackend.Services.ItemService
{
    public interface IItemService
    {
        Task<Item?> GetItem(int id);

        Task<List<Item>> GetItemsByGroupId(int groupId);

        Task<Item> AddItem(Item item);

        Task<Item?> UpdateItem(int id, Item item);

        Task<Item?> DeleteItem(int itemId);

    }
}
