﻿using Microsoft.EntityFrameworkCore;

namespace ToDoAppBackend.Services.GroupService
{
    public class GroupService : IGroupService
    {
        private readonly DataContext _dataContext;

        public GroupService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Group> AddGroup(Group group)
        {
            _dataContext.Groups.Add(group);
            await _dataContext.SaveChangesAsync();
            return group;
        }

        public async Task<Group?> DeleteGroup(int id)
        {
            var group = await _dataContext.Groups.FindAsync(id);
            if (group is null)
            {
                return null;
            }
            _dataContext.Groups.Remove(group);
            await _dataContext.SaveChangesAsync() ;
            return group;
        }

        public async Task<Group?> GetGroupById(int id)
        {
            var group = await _dataContext.Groups.FindAsync(id);
            if (group is null)
            {
                return null;
            }
            return group;
        }

        public async Task<List<Group>> GetGroupsByUserId(int userId)
        {
            var groups = await _dataContext.Groups.Where(g => g.UserId == userId).ToListAsync();
            return groups;
        }

        public async Task<List<Group>> GetGroups()
        {
            var groups = await _dataContext.Groups.ToListAsync();
            return groups;
        }

        public async Task<Group?> UpdateGroup(int id, Group request)
        {
            var group = await _dataContext.Groups.FindAsync(id);
            if (group is null) {
                return null;
            }

            group.Name = request.Name;

            await _dataContext.SaveChangesAsync();
            return group;
        }
    }
}
