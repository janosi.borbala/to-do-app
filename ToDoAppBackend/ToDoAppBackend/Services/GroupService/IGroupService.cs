﻿namespace ToDoAppBackend.Services.GroupService
{
    public interface IGroupService
    {
        Task<List<Group>> GetGroups();

        Task<Group?> GetGroupById(int id);

        Task<List<Group>> GetGroupsByUserId(int userId);

        Task<Group> AddGroup(Group group);

        Task<Group?> UpdateGroup(int id, Group group);

        Task<Group?> DeleteGroup(int id);

    }
}
