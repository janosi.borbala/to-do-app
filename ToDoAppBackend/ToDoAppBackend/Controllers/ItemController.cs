﻿using Microsoft.AspNetCore.Mvc;
using ToDoAppBackend.Services.ItemService;

namespace ToDoAppBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IItemService _itemService;

        public ItemController(IItemService itemService) {
            _itemService = itemService;
        }

        [HttpGet("groupId")]
        public async Task<ActionResult<List<Item>>> GetItemsByGroupId(int groupId) {
            var items = await _itemService.GetItemsByGroupId(groupId);
            return Ok(items);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Item>> GetItem(int id)
        {
            var item = await _itemService.GetItem(id);
            return Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<Item>> PostItem(Item iteam) {
            Item? newItem = await _itemService.AddItem(iteam);
            return Ok(newItem);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Item>> UpdateItem(int id, Item item) {
            Item newItem = new Item();
            newItem.Id = id;
            newItem.Name = item.Name;
            newItem.Description = item.Description;
            newItem.DueDate = item.DueDate;
            newItem.IsCompleted = item.IsCompleted;

            await _itemService.UpdateItem(id, newItem);
            return Ok(newItem);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult<Item>> UpdateItemIsCompleted(int id, Item item)
        {
            var oldItem = await _itemService.GetItem(id);
         
            if(oldItem == null)
            {
                return BadRequest("Item does not exists");
            }
            oldItem.IsCompleted = item.IsCompleted;

            await _itemService.UpdateItem(id, oldItem);
            return Ok(oldItem);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItem(int id) { 
            var result = await _itemService.DeleteItem(id);
            if (result is null)
                return NotFound();
            return NoContent();
        }
    }
}
