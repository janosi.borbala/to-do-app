﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ToDoAppBackend.Services.GroupService;
using ToDoAppBackend.Services.ItemService;

namespace ToDoAppBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService _groupService;

        public GroupController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Group>>> GetGroups() { 
            var groups = await _groupService.GetGroups();
            return Ok(groups);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Group>> GetGroup(int id)
        {
            var group = await _groupService.GetGroupById(id);
            return Ok(group);
        }

        [HttpGet("userId")]
        public async Task<ActionResult<List<Group>>> GetGroupsByUserId(int userId)
        {
            var groups = await _groupService.GetGroupsByUserId(userId);
            return Ok(groups);
        }

        [HttpPost]
        public async Task<ActionResult<Group>> PostGroup(Group group) {
            if (group == null)
            {
                return BadRequest("Group can't be null");
            }
            Group newGroup = new Group();
            newGroup.Name = group.Name;
            newGroup.UserId = group.UserId;
            newGroup = await _groupService.AddGroup(newGroup);

            return Ok(newGroup);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Group>> UpdateGroup(int id, Group group) {
            if (group == null)
            {
                return BadRequest("Group can't be null");
            }
            Group newGroup = new Group();
            newGroup.Name = group.Name;
            newGroup.UserId = group.UserId;
            newGroup.Id = group.Id;
            newGroup = await _groupService.UpdateGroup(id, newGroup);

            return Ok(newGroup);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroup(int id) { 
            var result = await _groupService.DeleteGroup(id);
            if (result is null)
                return NotFound();
            return NoContent();
        }
    }
}
