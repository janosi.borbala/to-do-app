﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ToDoAppBackend.Services.UserService;

namespace ToDoAppBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController: ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public UserController(IUserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        private string CreateToken(User user)
        {
            List<Claim> claims;
            
            claims = new List<Claim> {
            new Claim(ClaimTypes.Name , user.UserName),
            new Claim(ClaimTypes.Role, "User"),
            };
            

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Token").Value!));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds
                );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

        [HttpPost("register")]
        public async Task<ActionResult<User>> Register(User request)
        {

            /*String message = await ValidateUser(request);
            if (message != "")
            {
                return BadRequest(message);
            }*/

            string passwordHash = BCrypt.Net.BCrypt.HashPassword(request.Password);
            User user = new User();
            user.UserName = request.UserName;
            user.Password = passwordHash;
            
            await _userService.AddUser(user);
            return Ok();
        }

        public class LoginResponse
        {
            public string? Token { get; set; }
            public int Id { get; set; }

            public string? Username { get; set; }

        }

        [HttpPost("login")]
        public async Task<ActionResult<LoginResponse>> Login(User request)
        {
            /*String message = await ValidateLogin(request);
            if (message != "")
            {
                return BadRequest(message);
            }*/

            string passwordHash = BCrypt.Net.BCrypt.HashPassword(request.Password);
            User user = await _userService.GetUserByUsername(userName: request.UserName!)!;

            if (!BCrypt.Net.BCrypt.Verify(request.Password, user.Password))
            {
                return BadRequest("Username or password incorrect!");
            }
            string token = CreateToken(user);

            var resp = new LoginResponse
            {
                Token = token,
                Id = user.Id,
                Username = user.UserName,
            };
            return Ok(resp);

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var result = await _userService.DeleteUser(id);
            if (result is null)
                return NotFound();
            return NoContent();
        }
    }
}
