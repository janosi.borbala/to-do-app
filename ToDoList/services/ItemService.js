import { itemUrl } from "../config/urls";

export default async function getItemsByGroupId(groupId) {
    return fetch(`${itemUrl}/groupId?groupId=${groupId}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            return response.status;
        })
        .catch((error) => {
            console.log(error);
        });
}

export async function postItem(item) {
    return fetch(itemUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
    })
        .catch((error) => {
            console.log(error);
        });
}

export async function deleteItem(id) {
    return fetch(`${itemUrl}/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.status;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  export async function patchItem(id, item) {
    return fetch(`${itemUrl}/${id}`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
    })
        .catch((error) => {
            console.log(error);
        });
}