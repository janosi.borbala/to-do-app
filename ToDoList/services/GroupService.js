import { groupUrl } from "../config/urls";

export default async function getGroupsByUserId(userId) {

    return fetch(`${groupUrl}/userId?userId=${userId}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            return response.status;
        })
        .catch((error) => {
            console.log(error);
        }
        );
}

export async function postGroup(group) {
    return fetch(groupUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(group),
    })
        .catch((error) => {
            console.log(error);
        });
}

export async function deleteGroup(id) {
    return fetch(`${groupUrl}/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.status;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  export async function putGroup(id, group) {
    return fetch(`${groupUrl}/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(group),
    })
        .catch((error) => {
            console.log(error);
        });
}