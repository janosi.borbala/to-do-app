import { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TextInput, Pressable } from "react-native";

import { postItem } from '../services/ItemService';

const AddItem = ({ route, navigation }) => {
    const { groupId } = route.params;
    const [date, setDate] = useState(new Date())
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')

    async function submitItem() {
        var item = {
            name: name,
            description: description,
            isCompleted: false,
            groupId: groupId,
        }
        var i = await postItem(item);
        if (i.status == 200) {
            navigation.navigate("To do list", { refresh: true },);
        }
    }

    useEffect(() => {
    }, []);

    return (
        <View style={styles.box}>
            <View>
                <TextInput
                    placeholder="Name"
                    style={styles.textinput}
                    onChangeText={newname => setName(newname)}
                    defaultValue={name}
                />
                <TextInput
                    placeholder="Description"
                    style={styles.descriptioninput}
                    multiline={true}
                    onChangeText={newdescription => setDescription(newdescription)}
                    defaultValue={description}
                />

                <Pressable
                    style={styles.submit}
                    onPress={submitItem}
                >
                    <Text>Add task</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default AddItem;

const styles = StyleSheet.create({
    box: {
        width: '90%',
        height: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DBD3D8',
        borderRadius: 20,
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: "rgba(214, 214, 214, 1.0)",
        marginHorizontal: 20,
        marginVertical: 20,
        flexDirection: "column"
    },
    textinput: {
        backgroundColor: 'white',
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        borderRadius: 10,
        height: 40,
        width: 300,
        textAlign: 'center',

    },
    descriptioninput: {
        backgroundColor: 'white',
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        width: '100%',
        borderRadius: 10,
        height: 150,
        width: 300,
        marginVertical: 30,
    },
    submit: {
        backgroundColor: "#6D676E",
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        color: "white",
        fontSize: 18,
    },
});