import { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, Pressable } from 'react-native';
import getItemsByGroupId, { deleteItem, patchItem } from '../services/ItemService.js';


const ItemPage = (props) => {
    const { groupId } = props;

    const [items, setItems] = useState([]);

    async function loadItems() {
        let i = await getItemsByGroupId(groupId)
        if (i != undefined) {
            setItems(i)
        }

    }

    async function removeItem(id) {
        await deleteItem(id);
        loadItems().catch(console.error);
    }

    async function completeItem(id, checked) {
        var item = {
            isCompleted: checked
        }
        await patchItem(id, item)
        loadItems().catch(console.error);
    }

    useEffect(() => {
        loadItems().catch(console.error);
    }, []);

    return (
        <View style={{ width: '100%' }}>
            {items.map((item) => (
                <View style={styles.box} key={item.id}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{item.name}</Text>
                    {item.description != '' && <Text>Description: {item.description}</Text>}
                    {(new Date(item.dueDate).getFullYear() != new Date('0001-01-01').getFullYear()) && <Text>Duedate: {item.dueDate}</Text>}
                    {item.isCompleted &&
                        <Pressable
                            style={{ marginTop: 10 }}
                            onPress={() => completeItem(item.id, false)}
                        >
                            <Image
                                source={require('../assets/check-box.png')}
                                alt='checked'
                                style={styles.icon}
                            />
                        </Pressable>
                    }
                    {!item.isCompleted &&
                        <Pressable
                            style={{ marginTop: 10 }}
                            onPress={() => completeItem(item.id, true)}
                        >
                            <Image
                                source={require('../assets/check-box-empty.png')}
                                alt='checked' style={styles.icon}
                            />
                        </Pressable>
                    }
                    <Pressable
                        style={{ marginTop: 10 }}
                        onPress={() => removeItem(item.id)}
                    >
                        <Image
                            source={require('../assets/trash.png')}
                            alt='checked' style={styles.icon}
                        />
                    </Pressable>
                </View>
            ))}

        </View>
    );
}

export default ItemPage;

const styles = StyleSheet.create({
    box: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DBD3D8',
        borderRadius: 20,
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: "rgba(214, 214, 214, 1.0)",
        marginBottom: 10,
        color: 'white',
        fontSize: 60
    },
    icon: {
        height: '10%',
        width: '10%',
        padding: 20,
    },
});