import { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, Pressable, TextInput, ScrollView } from 'react-native';
import getGroupsByUserId, { deleteGroup, postGroup, putGroup } from '../services/GroupService.js';
import getItemsByGroupId from '../services/ItemService.js';
import ItemPage from './ItemPage.jsx';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Dimensions } from 'react-native';

function LandingPage({ route, navigation }) {
    //const { refresh } = route.params;
    const [groups, setGroups] = useState([]);
    const [items, setItems] = useState([]);
    const [name, setName] = useState('')
    const [groupName, setGroupName] = useState('')
    const [editGroupId, setEditGroupId] = useState(-1)

    async function loadItems(groupId) {
        let i = await getItemsByGroupId(groupId)

        let it = [{groupId: i}]
        
        setItems(items.push(i))
    }

    async function loadData() {
        let g = await getGroupsByUserId(1)
        if (g != undefined) {
            setGroups(g)
            g.forEach(group => {
                loadItems(group.id)
            });
        }
    }

    async function removeGroup(id) {
        await deleteGroup(id)
        loadData().catch(console.error);
    }

    async function addGroup() {
        if (name != '') {
            var group = {
                name: name,
                userId: 1
            }
            await postGroup(group);
            setName('')
            loadData().catch(console.error);
        }
    }

    function showEditBar(id) {
        if (editGroupId === id) {
            setEditGroupId(-1);
        }
        else {
            setEditGroupId(id);
        }
    }

    async function renameGroup(id) {
        if (groupName != '') {
            var group = {
                userId: 1,
                name: groupName
            }
            let p = await putGroup(id, group);
            setGroupName('')
            setEditGroupId(-1)
            loadData().catch(console.error);
        }
    }

    useEffect(() => {
        loadData().catch(console.error);

        if (route.params?.refresh) {
            loadData();
        }
    }, [route.params?.refresh]);

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <View style={styles.heading}>
                    <TextInput
                        style={styles.textinput}
                        placeholder="Add new group"
                        onChangeText={setName}
                        value={name}
                    />
                    <Pressable onPress={() => addGroup()}>
                        <Image
                            source={require('../assets/add.png')}
                            alt='addgroup'
                            style={styles.headingicon}
                        />
                    </Pressable>
                </View>
                {groups.map((group) => (
                    <View style={styles.box} key={group.id}>
                        <View style={styles.groupheading}>
                            <Text style={styles.headingtext}>{group.name}</Text>
                            <Pressable onPress={() => removeGroup(group.id)}>
                                <Image
                                    source={require('../assets/trash.png')}
                                    alt='trashcan' style={styles.headingicon}
                                />
                            </Pressable>
                            <Pressable onPress={() => showEditBar(group.id)}>
                                <Image
                                    source={require('../assets/editing.png')}
                                    alt='edit'
                                    style={styles.headingicon}
                                />
                            </Pressable>
                        </View>
                        {editGroupId == group.id &&
                            <View style={styles.groupheading}>
                                <TextInput
                                    style={styles.textinput}
                                    placeholder="New group name"
                                    onChangeText={setGroupName}
                                    value={groupName}
                                />
                                <Pressable onPress={() => renameGroup(group.id)}>
                                    <Image
                                        source={require('../assets/check.png')}
                                        alt='rename'
                                        style={styles.headingicon}
                                    />
                                </Pressable>
                            </View>
                        }
                        <View style={styles.rectangle}>
                            <ItemPage groupId={group.id}></ItemPage>
                            <View style={styles.addiconbox} >
                                <Pressable onPress={() => navigation.navigate('Add new item', { groupId: group.id })}>
                                    <Image
                                        source={require('../assets/add.png')}
                                        alt='add' style={styles.icon}
                                    ></Image>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                ))}
            </ScrollView>
        </SafeAreaView>

    );
}

export default LandingPage;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eff1f3',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },

    contentContainer: {
        width: Dimensions.get('window').width,
        flexgrow: 1,
    },

    box: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: 10,
        flex: 0.2
    },

    rectangle: {
        width: '90%',
        height: '70%',
        alignItems: 'center',
        padding: 1,
        minHeight: 300,
    },

    heading: {
        width: '90%',
        minHeight: 50,
        height: '4%',
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: "rgba(214, 214, 214, 1.0)",
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 10,
        marginLeft: 17,
        fontSize: 60,
        backgroundColor: '#DBD3D8',
        color: 'white'
    },

    groupheading: {
        width: '90%',
        height: '10%',
        minHeight: 50,
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: "rgba(214, 214, 214, 1.0)",
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 10,
        marginTop: 10,
        fontSize: 60,
        backgroundColor: '#DBD3D8',
        color: 'white'
    },

    icon: {
        width: '10%',
        height: '10%',
        padding: 20,
    },

    headingicon: {
        width: '10%',
        height: '10%',
        padding: 20,
        marginRight: 5
    },

    headingtext: {
        flex: 2,
        marginLeft: 20,
        fontSize: 15,
        fontWeight: 'bold'
    },

    textinput: {
        flex: 2,
        marginHorizontal: 20,
        fontSize: 15,
        fontWeight: 'bold',
        backgroundColor: '#eff1f3',
        height: '60%',
        borderRadius: 4,
        textAlign: 'center',
    },

    addiconbox: {
        width: '18%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DBD3D8',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: "rgba(214, 214, 214, 1.0)",
        alignItems: 'center',
        justifyContent: 'center'
    },
});