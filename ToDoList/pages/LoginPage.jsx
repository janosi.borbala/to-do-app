import { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TextInput, Pressable } from "react-native";

const LoginPage = ({ route, navigation }) => {
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')

    useEffect(() => {
    }, []);

    return (
        <View style={styles.box}>
            <View>
                <TextInput
                    placeholder="Name"
                    style={styles.textinput}
                    onChangeText={newname => setName(newname)}
                    defaultValue={name}
                />
                <TextInput
                    placeholder="Password"
                    style={styles.textinput}
                    onChangeText={password => setPassword(password)}
                    defaultValue={password}
                    secureTextEntry={true}
                />
                <Pressable
                    style={styles.submit}
                >
                    <Text>Login</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default LoginPage;

const styles = StyleSheet.create({
    box: {
        width: '90%',
        height: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DBD3D8',
        borderRadius: 20,
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: "rgba(214, 214, 214, 1.0)",
        marginHorizontal: 20,
        marginVertical: 20,
        flexDirection: "column"
    },
    textinput: {
        backgroundColor: 'white',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        borderRadius: 10,
        height: 40,
        width: 300,
        textAlign: 'center',

    },
    submit: {
        backgroundColor: "#6D676E",
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        color: "white",
        fontSize: 18,
        margin: 10
    },
});